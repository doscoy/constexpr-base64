#pragma once

#include <cassert>
#include <cstddef>
#include <iterator>
#include <stdexcept>
#include <tuple>
#include <utility>

namespace container {

template <class T, std::size_t N>
struct array {
  using value_type = T;
  using reference = value_type&;
  using const_reference = const value_type&;
  using iterator = value_type*;
  using const_iterator = const value_type*;
  using pointer = value_type*;
  using const_pointer = const value_type*;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  value_type raw[N];
  const size_type len;

  constexpr iterator begin() noexcept { return iterator(data()); }
  constexpr const_iterator begin() const noexcept {
    return const_iterator(data());
  }
  constexpr iterator end() noexcept { return iterator(data() + len); }
  constexpr const_iterator end() const noexcept {
    return const_iterator(data() + len);
  }
  constexpr reverse_iterator rbegin() noexcept {
    return reverse_iterator(end());
  }
  constexpr const_reverse_iterator rbegin() const noexcept {
    return const_reverse_iterator(end());
  }
  constexpr reverse_iterator rend() noexcept {
    return reverse_iterator(begin());
  }
  constexpr const_reverse_iterator rend() const noexcept {
    return const_reverse_iterator(begin());
  }
  constexpr const_iterator cbegin() const noexcept { return begin(); }
  constexpr const_iterator cend() const noexcept { return end(); }
  constexpr const_reverse_iterator crbegin() const noexcept { return rbegin(); }
  constexpr const_reverse_iterator crend() const noexcept { return rend(); }

  constexpr size_type size() const noexcept { return len; }
  constexpr size_type max_size() const noexcept { return len; }
  [[nodiscard]] constexpr bool empty() const noexcept { return len == 0; }

  constexpr reference operator[](size_type n) noexcept {
    assert(n < len);
    return raw[n];
  }
  constexpr const_reference operator[](size_type n) const noexcept {
    assert(n < len);
    return raw[n];
  }

  constexpr reference at(size_type n) {
    if (n >= len) throw std::out_of_range("contaier::at");
    return raw[n];
  }
  constexpr const_reference at(size_type n) const {
    if (n >= len) throw std::out_of_range("contaier::at");
    return raw[n];
  }

  constexpr reference front() noexcept { return *begin(); }
  constexpr const_reference front() const noexcept { return *begin(); }
  constexpr reference back() noexcept { return *(end() - 1); }
  constexpr const_reference back() const noexcept { return *(end() - 1); }

  constexpr value_type* data() noexcept { return raw; }
  constexpr const value_type* data() const noexcept { return raw; }
};

}  // namespace container

namespace concat {

namespace detail {

template <int, std::size_t...>
class sum_sequence;

template <int Tune, std::size_t I1, std::size_t I2, std::size_t... Tail>
class sum_sequence<Tune, I1, I2, Tail...> {
  template <std::size_t... I>
  static inline constexpr auto apend(std::integer_sequence<std::size_t, I...>) {
    return std::integer_sequence<std::size_t, I1 + Tune, I...>();
  }

 public:
  using get = decltype(apend(
      typename sum_sequence<Tune, I1 + I2 + Tune, Tail...>::get()));
  static inline constexpr auto max() {
    return sum_sequence<Tune, I1 + I2 + Tune, Tail...>::max();
  }
};

template <int Tune, std::size_t I1, std::size_t I2>
class sum_sequence<Tune, I1, I2> {
 public:
  using get = std::integer_sequence<std::size_t, I1 + Tune, I1 + I2 + 2 * Tune>;
  static inline constexpr auto max() { return I1 + I2 + 2 * Tune; }
};

template <int Tune, std::size_t I1>
class sum_sequence<Tune, I1> {
 public:
  using get = std::integer_sequence<std::size_t, I1 + Tune>;
  static inline constexpr auto max() { return I1 + Tune; }
};

template <int Tune, class T, std::size_t... N>
struct memo {
  using value_type = T;

  const T* const ptr[sizeof...(N) + 1];
  const std::size_t sum[sizeof...(N) + 1];

  constexpr memo(const value_type (&... arr)[N]) noexcept
      : memo(typename sum_sequence<Tune, N...>::get(), arr...) {}

  static constexpr auto size() {
    if constexpr (sizeof...(N) < 256)
      return (... + N) + sizeof...(N) * (Tune);
    else
      return sum_sequence<Tune, N...>::max();
  }
  static constexpr auto memo_size() { return sizeof...(N); }

 private:
  template <std::size_t... SumSeq>
  constexpr memo(std::index_sequence<SumSeq...>,
                 const value_type (&... arr)[N]) noexcept
      : ptr{nullptr, arr...}, sum{0, SumSeq...} {}
};

template <int Tune = 0, class T, std::size_t... N>
static inline constexpr auto make_memo(const T (&... arr)[N]) {
  return memo<Tune, T, N...>{arr...};
}

template <bool ApendNull = false, class T, std::size_t... I>
static inline constexpr auto make_arr(const T& memo,
                                      std::index_sequence<I...>) noexcept {
  std::size_t p1 = 1, p2 = -1;
  if constexpr (ApendNull) {
    using type = typename T::value_type;
    using arr = container::array<type, T::size() + 1>;
    return arr{
        {(I < memo.sum[p1] ? memo.ptr[p1][++p2] : memo.ptr[++p1][p2 = 0])...,
         type{}},
        T::size()};
  } else {
    using arr = container::array<typename T::value_type, T::size()>;
    return arr{
        {(I < memo.sum[p1] ? memo.ptr[p1][++p2] : memo.ptr[++p1][p2 = 0])...},
        T::size()};
  }
}

template <int Tune = 0, bool ApendNull = false, class T, std::size_t... N>
static inline constexpr auto pass_maker(const T (&... arr)[N]) {
  using memo = memo<Tune, T, N...>;
  using seq = std::make_index_sequence<memo::size()>;
  return make_arr<ApendNull>(memo{arr...}, seq{});
}

}  // namespace detail

template <class T, std::size_t... N>
static inline constexpr auto data(const T (&... data)[N]) noexcept {
  return detail::pass_maker<0, false>(data...);
}

template <class T, std::size_t... N>
static inline constexpr auto string(const T (&... str)[N]) noexcept {
  return detail::pass_maker<-1, true>(str...);
}

}  // namespace concat

namespace base64 {

using char_type = char;
using byte_type = unsigned char;

template <std::size_t N>
using string = container::array<char_type, N>;

template <std::size_t N>
using bytes = container::array<byte_type, N>;

namespace detail {
#ifdef BASE64_USING_HUGE_CONVERSION_TABLE
#include "tabledata"
#endif

static inline constexpr char_type kEncodeTable[]{
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
static inline constexpr byte_type kDecodeTable[]{
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  62, 0,  0,  0,  63, 52, 53, 54, 55, 56, 57, 58, 59, 60,
    61, 0,  0,  0,  0,  0,  0,  0,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,
    11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0,  0,  0,  0,
    0,  0,  26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
    43, 44, 45, 46, 47, 48, 49, 50, 51, 0,  0,  0,  0,  0};

static inline constexpr auto encode_edge_value(
    const byte_type* const beg, const byte_type* const end,
    const std::size_t seq_size) noexcept {
  const auto data_len{static_cast<std::size_t>(end - beg)};
  const auto edge{seq_size * 3 / 4};
  if (data_len == edge + 3) {
    return std::make_tuple(
        kEncodeTable[((end[-3] >> 2)) & 0b111111],
        kEncodeTable[((end[-3] << 4) | (end[-2] >> 4)) & 0b111111],
        kEncodeTable[((end[-2] << 2) | (end[-1] >> 6)) & 0b111111],
        kEncodeTable[((end[-1] << 0)) & 0b111111]);
  } else if (data_len == edge + 2) {
    return std::make_tuple(
        kEncodeTable[((end[-2] >> 2)) & 0b111111],
        kEncodeTable[((end[-2] << 4) | (end[-1] >> 4)) & 0b111111],
        kEncodeTable[((end[-1] << 2)) & 0b111111], '=');
  } else if (data_len == edge + 1) {
    return std::make_tuple(
        kEncodeTable[((end[-1] >> 2)) & 0b111111],
        kEncodeTable[((end[-1] << 4)) & 0b111111], '=', '=');
  } else {
    return std::make_tuple('=', '=', '=', '=');
  }
}

template <std::size_t... I>
static inline constexpr auto make_from_array(
    const byte_type* const beg, const byte_type* const end,
    std::index_sequence<I...>) noexcept {
  using string = string<sizeof...(I) + 5>;
  const auto [d0, d1, d2, d3]{encode_edge_value(beg, end, sizeof...(I))};
#ifdef BASE64_USING_HUGE_CONVERSION_TABLE
  return string{
      {kHugeEncodeTable[I % 4][beg[I * 3 / 4 + 0]][beg[I * 3 / 4 + 1]]...,
       d0, d1, d2, d3, '\0'},
      sizeof...(I) + 4};
#else
  using T = typename string::value_type;
  return string{
      {static_cast<T>(
           I % 4 == 0   ? kEncodeTable[((beg[I * 3 / 4 + 0] >> 2)) & 0b111111]
           : I % 4 == 1 ? kEncodeTable[((beg[I * 3 / 4 + 0] << 4) |
                                        (beg[I * 3 / 4 + 1] >> 4)) &
                                       0b111111]
           : I % 4 == 2 ? kEncodeTable[((beg[I * 3 / 4 + 0] << 2) |
                                        (beg[I * 3 / 4 + 1] >> 6)) &
                                       0b111111]
           : I % 4 == 3 ? kEncodeTable[((beg[I * 3 / 4 + 0] << 0)) & 0b111111]
                        : 0)...,
       d0, d1, d2, d3, '\0'},
      sizeof...(I) + 4};
#endif
}

static inline constexpr auto decode_length_offset(
    const char_type* const end) noexcept {
  if (end[-4] == '=') {
    return 3;
  } else if (end[-3] == '=') {
    return 2;
  } else if (end[-2] == '=') {
    if ((kDecodeTable[end[-3] & 0b1111111] & 0b1111) == 0) {
      return 2;
    } else {
      return 1;
    }
  } else if (end[-1] == '=') {
    if ((kDecodeTable[end[-2] & 0b1111111] & 0b0011) == 0) {
      return 1;
    } else {
      return 0;
    }
  } else {
    return 0;
  }
}

template <std::size_t... I>
static inline constexpr auto make_from_array(
    const char_type* const beg, const char_type* const end,
    std::index_sequence<I...>) noexcept {
  using bytes = bytes<sizeof...(I)>;
#ifdef BASE64_USING_HUGE_CONVERSION_TABLE
  return bytes{
      {kHugeDecodeTable[I % 3][static_cast<unsigned char>(beg[I / 3 + I + 0])]
                       [static_cast<unsigned char>(beg[I / 3 + I + 1])]...},
      sizeof...(I) - decode_length_offset(end)};
#else
  using T = typename bytes::value_type;
  return bytes{{static_cast<T>((kDecodeTable[beg[I / 3 + I + 0] & 0b1111111]
                                << (((I % 3) + 1) * 2)) |
                               (kDecodeTable[beg[I / 3 + I + 1] & 0b1111111] >>
                                ((2 - (I % 3)) * 2)))...},
               sizeof...(I) - decode_length_offset(end)};
#endif
}

}  // namespace detail

template <std::size_t N>
static inline constexpr auto encode(const byte_type* const bytes) noexcept {
  static_assert(N != 0, "invalid data length");
  constexpr auto kPaddingLen{N % 3 == 0 ? N : N + (3 - N % 3)};
  constexpr auto kSeqLen{(kPaddingLen - 1) / 3 * 4};
  return detail::make_from_array(bytes, &bytes[N],
                                 std::make_index_sequence<kSeqLen>());
}

template <std::size_t... N>
static inline constexpr auto encode(const byte_type (&... bytes)[N]) noexcept {
  static_assert(sizeof...(N) > 0, "need some data");
  if constexpr (sizeof...(N) == 1) {
    constexpr auto kLen{(N + ...)};
    return encode<kLen>(static_cast<const byte_type* const>(bytes)...);
  } else {
    return encode(concat::data(bytes...).raw);
  }
}

template <std::size_t N>
static inline constexpr auto decode(const char_type* const base64) noexcept {
  static_assert(N % 4 == 0, "invalid length");
  constexpr auto kLen{N / 4 * 3};
  return detail::make_from_array(base64, &base64[N],
                                 std::make_index_sequence<kLen>());
}

template <std::size_t... N>
static inline constexpr auto decode(const char_type (&... base64)[N]) noexcept {
  static_assert(sizeof...(N) > 0, "need some base64 code");
  if constexpr (sizeof...(N) == 1) {
    constexpr auto kLen{(N + ...)};
    return decode<kLen - 1>(static_cast<const char_type* const>(base64)...);
  } else {
    return decode(concat::string(base64...).raw);
  }
}

}  // namespace base64