# constexpr-base64

コンパイル時にbase64エンコード/デコードする

## 使い方

```c++
// ヘッダのインクルード前にBASE64_USING_HUGE_CONVERSION_TABLEを定義することで
// エンコード・デコードにアホみたいな変換テーブルを使用する
// 使用することで巨大なデータをエンコード・デコードする際のメモリ消費が抑えられるかもしれない
// #define BASE64_USING_HUGE_CONVERSION_TABLE
#include "base64.hpp"

constexpr unsigned char data1[]{0xDE, 0xAD, 0xBE, 0xEF};
constexpr unsigned char data2[]{0xBA, 0xBE};
constexpr unsigned char data3[]{0x64, 0x6F, 0x73, 0x63, 0x6F, 0x79};

// データ配列を直接渡す
constexpr auto encode{base64::encode(data1)};

// データ配列を複数渡すと結合される
constexpr auto encode{base64::encode(data1, data2, data3)};

// データの長さとポインタを渡す
constexpr auto encode{base64::encode<4>(&data1[0])};

// 文字列リテラルを直接渡す
constexpr auto decode{base64::decode("Y29uc3RleHByLWJhc2U2NA==")};

// 文字列リテラルを区切って渡すと結合される
// コードが文字列リテラルの最大長より大きい場合でも適当に区切ればデコードできる
constexpr auto decode{base64::decode("Y29uc", "3RleHBy", "LWJhc2", "U2NA==")};

// コードの長さとポインタを渡す
constexpr char code[]{"Y29uc3RleHByLWJhc2U2NA=="};
constexpr auto decode{base64::decode<24>(&code[0])};

// std::arrayと同じように使用できる
for(auto e: encode) std::cout << e;
for(auto d: decode) std::cout << d;
constexpr auto e_zero{encode[0]};
constexpr auto d_zero{decode[0]};
constexpr auto e_size{encode.size()};
constexpr auto d_size{decode.size()};
constexpr auto e_data{encode.data()};
constexpr auto d_data{decode.data()};
```