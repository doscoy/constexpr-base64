#include <iostream>
#include <iomanip>

// #define BASE64_USING_HUGE_CONVERSION_TABLE
#include "base64.hpp"

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv) {
  constexpr auto src{concat::string(
      "44GT44KT44Gq44Gr5Zue44KK44GP44Gp44GE44GT44Go44KS44GZ44KL44KI44KKY3N244OV44Kh",
      "44Kk44Or44KSaW5jbHVkZeOBl+OBpumFjeWIl+OCkuWIneacn+WMluOBl+OBn+aWueOBjOOCiOOB",
      "o+OBveOBqemAn+OBhOOBl+ODoeODouODqua2iOiyu+OCguWwkeOBquOBhA==")};
  constexpr auto decode{base64::decode<src.size()>(src.data())};
  constexpr auto encode{base64::encode<decode.size()>(decode.data())};
  constexpr auto re_decode{base64::decode<encode.size()>(encode.data())};

  static_assert(src.size() == encode.size());
  static_assert(decode.size() == re_decode.size());

  std::cout << "src -> decode -> encode\n  ";
  for (std::size_t i = 0; i < src.size();
       ++i, std::cout << (i % 76 == 0 ? "\n  " : "")) {
    if (src[i] == encode[i]) {
      std::cout << encode[i];
    } else {
      std::cout << "\033[31m" << encode[i] << "\033[0m";
      std::cout << "\nmiss...\n";
      return -1;
    }
  }
  std::cout << "(" << encode.size() << "bytes)\n\n";

  std::cout << "decode -> encode -> decode\n  ";
  std::cout << std::hex << std::uppercase;
  for (std::size_t i = 0; i < decode.size();
       ++i, std::cout << (i % 16 == 0 ? "\n  " : " ")) {
    if (decode[i] == re_decode[i]) {
      std::cout << std::setfill('0') << std::setw(2)
                << static_cast<unsigned>(re_decode[i]);
    } else {
      std::cout << "\033[31m" << std::setfill('0') << std::setw(2)
                << static_cast<unsigned>(re_decode[i]) << "\033[0m";
      std::cout << "\nmiss...\n";
      return -1;
    }
  }
  std::cout << "(" << std::dec << re_decode.size() << "bytes)\n\n";

  std::cout << re_decode.data() << "\n";
}